---
layout: handbook-page-toc
title: Mentoring at GitLab
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What is mentoring?

Mentor relationships are an opportunity for individuals to learn from someone's personal experience, background, and perspective. These relationships build trust on a team, provide safe space to make mistakes, and encourage both personal and professional development.

Mentorship is an opportunity for both the mentor and mentee to develop their leadership and communication skills. Mentorship should be led by the mentee, similar to how [1:1's at GitLab](/handbook/leadership/1-1/) are driven by direct reports.

As of 2021-10-25, 125 team members have participated in formal mentorship programs organized at GitLab. This includes 43 people in the Minorities in Tech mentorship program and 82 in the Women at GitLab mentorship program (first and second iteration combined).

***The Power of Mentoring***
<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/0W3d-PJ4-FM" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

### Questions

Share questions in the [#mentoring slack channel](https://app.slack.com/client/T02592416/C01QKNDJ76J).

## Benefits of mentorship

**Benefits for the mentee**

1. Be encouraged to prioritize, and be held accountable for, your [career development](/handbook/people-group/learning-and-development/career-development/)
1. Learn new skills related to your current role, your future career goals, or an area that you're passionate about
1. Set and reach clearly outlined [goals](/company/okrs/).

**Benefits for the mentor**

1. Serve as a [leader in the organization](/handbook/leadership/) by enabling growth for other team members
1. Practice leadership, [effective communication](/handbook/communication/), and [coaching](/handbook/leadership/coaching/) skills
1. Establish yourself as an expert in a field or speciality
1. [Build trust](/handbook/leadership/building-trust/) with team members

## Find a mentor

The following team members are available as mentors. Schedule a [coffee chat](/company/culture/all-remote/informal-communication/#coffee-chats) to get the conversation started!

The `Expertise` column comes from the team member .yaml entry and might not include all subjects they can help you with.

<%= mentors %>

## Become a mentor

1. Team members can designate their availability to be a mentor on the GitLab team page by setting the `mentor` status to `true`. This will appear on the team page with a line that reads `Available as a mentor`. [Example MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/75890)

## Organized mentorship programs

Learn more about organized mentorship programs for team members:

1. [Company-wide mentorship program](/handbook/people-group/learning-and-development/mentor/company-program)
1. [Minorities in Tech Mentoring Program](/company/culture/inclusion/erg-minorities-in-tech/mentoring/)
1. [Women's TMRG sponsored program](/company/culture/inclusion/tmrg-gitlab-women/mentorship-program/)
1. [Aspiring Manager mentorship pilot program](/handbook/engineering/development/dev/training/ic-to-manager/#aspiring-manager-mentorship-pilot-program)
1. The [Engineering department](/handbook/engineering/) outlines [strategies for mentors and mentees](/handbook/engineering/career-development/mentoring/) with suggestions on how to host meetings and set and evaluate goals.
1. The [Minorities in Tech TMRG](/company/culture/inclusion/erg-minorities-in-tech/) organized a [mentor program](/company/culture/inclusion/erg-minorities-in-tech/mentoring/) with the goal of enabling GitLab to support its Diversity value.
1. The [Sales department](/handbook/sales/) organized a pilot [Women in Sales Mentorship Program](/handbook/people-group/women-in-sales-mentorship-pilot-program/#women-in-sales-mentorship-program-pilot). The program benefits are outlined [here](/handbook/people-group/women-in-sales-mentorship-pilot-program/#program-benefits).
1. The [Support team](/handbook/support/) has outlined expectations and examples on [Mentorship in Support Engineering](/handbook/support/engineering/mentorship.html).
1. The [Finance team](/handbook/finance/) is running a [mentorship program](/handbook/finance/growth-and-development/mentorship).

Consider using the Minorities in Tech TMRG mentorship [program structure](/company/culture/inclusion/erg-minorities-in-tech/mentoring/program-structure/) to organize a mentor program for your team or TMRG.

## Expectations of mentors and mentees

Adapted from the [NCWIT Mentoring-in-a-Box Mentoring Basics - A Mentor's Guide to Success](https://www.ncwit.org/sites/default/files/legacy/pdf/IMentor_MentorGuide.pdf), section What Are the "Dos" of Mentoring and [People Grove resources](https://support.peoplegrove.com/hc/en-us/articles/360001265792-Structure-Goals-and-Agendas):

| Expectation | Description |
| ----- | --------------- |
| **Maintain Boundaries** | Maintain clear and appropriate boundaries. Be clear on where the line is drawn between your responsibilities and those of their manager. | 
| **Set Goals** | Mentorship is a process with a goal. Mentees should create goals for themselves from the outset and put them in writing. Frequently revisit goals to measure progress. | 
| **Build Trust** | Act as a colleague first, an expert second. Spend time [getting to know one another.](/handbook/values/#diversity-inclusion). Use an open and warm tone to create a safe place to ask difficult questions and take risks. [Be open, honest, and fully authentic.](/handbook/values/#transparency) | 
| **Set Expectations** | Be realistic and uphold your commitment. Share access to resources and people, but make it clear you do not wield your influence over others – coach as you can but the mentee needs to do their own work.| 
| **Listen** | Establish [trust](/handbook/leadership/building-trust/) and openness in communication from the start. Give your full, undivided attention. Listen as much as you speak so that their questions and aspirations are always the central focus. Hear concerns before offering advice and guidance.| 
| **Acknowledge Independence** | Recognise that the mentee goals are their own and that they may have career goals that differ from the path you chose. Your role as a mentor is to guide; it’s up to the mentee to decide what to implement in their own career. | 
| **Respect Diverse Experiences** | Recognise that minorities within the organisation sometimes face additional barriers to advancement. Educate yourself about the issues and ask for advice and support via the [appropriate Diversity, Inclusion and Belonging channels](/company/culture/inclusion/erg-guide/#how-to-join-current-tmrgs-and-their-slack-channels). | 
| **Practice Self-Awareness** | Identify your strengths, weaknesses, and [biases.](/handbook/values/#unconscious-bias). Keep an open mind. Be aware of and respect other's experiences, ideas, and goals. Mentorship requires open dialogue about the ways gender and culture influence your mentee's work in the organisation and the mentoring relationship itself. | 
| **Advocate** | Educate others within the organisation about the advancement of women and other under-represented groups. Approach managers and other team members and mentor them on being effective managers or colleagues to those who might have different experiences to them. Teach your mentee how to become a mentor themselves – by example and by encouragement.| 
| **Express Gratitude** | Share openly about your experience. Express gratitude for the skills or experiences developed over time. | 
| **Maintain a Growth Mindset** |  Value a [growth mindset](/handbook/values/#growth-mindset) of both the mentor and mentee. Both parties can learn and grow from mentorship| 
| **Iterate** | Mentorship changes with time. [Iteration](/handbook/values/#iteration) of goals throughout the mentorship is encouraged. Focus on quick wins that the mentee can reach, then keep growing |

## Resources

Use these resources when participating in an organized mentor program or building your own mentor/mentee relationship. Adopt what fits and leave what doesn't resonate.

### Mentor and Mentee training

Take the LinkedIn Learning training called [How to be a Good Mentor and Mentee](https://www.linkedin.com/learning/how-to-be-a-good-mentee-and-mentor/the-power-of-mentoring?u=2255073). It covers strategies for finding a mentor, setting clear expectations, and achieving goals through mentorship. 

Content from this training has been adapted in the following [Google Slides presentation](https://docs.google.com/presentation/d/1QPx9ZGa051Jhwwfb78cKW1LD0uVTUdKxRdElBk4Ku9I/edit?usp=sharing) for learners who prefer to read the material at their own pace.

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vS3cZJcCIv_s44OfN9QLjje2wRqS7EwnrK3HCS_ZeT-ZGwk58hPq17L-c_DvCdvu0jxjR3r6yY8xY79/embed?start=false&loop=false&delayms=60000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

### Set your goal and purpose

In their book [Burnout: the secret to unlocking the stress cycle](https://www.burnoutbook.net/), authors Emily and Amelia Nagoski explain the difference between goals and purpose:

| Idea | Definition |
| ----- | --------------- |
| Goal | the `what` - outcome driven |
| Purpose | the `why` - value driven |

Mentors and mentees should have a foundational goal and purpose set for their mentorship. **Mentee goals should drive the relationship.**. Mentors should set goals based on that of their mentee.

Both mentors and mentees bring their own original purpose to mentorship. 

#### What is your goal?

| Critera | Description |
| ----- | --------------- |
| Time-bound | When will the goal be complete? |
| Certain | Is achievement of the goal within your control? |
| Specific | What is the desired outcome? |
| Positive | What positive impact will the outcome create? |
| Measurable | How will success be measured? |
| Personal | Why does achieving the goal matter? Tie goal to purpose. |

Using the matrix below, fill in each empy space based on the descriptions above to draft your goal:

| Time-bound | Certain | Specific | Positive | Measurable | Personal |
| -------- | -------- | -------- | -------- | -------- | -------- | 
| | | | | | |


_This structure is adapted from [Burnout: the secret to unlocking the stress cycle](https://www.burnoutbook.net/) and the [SMART goal framework](https://support.peoplegrove.com/hc/en-us/articles/360001265792-Structure-Goals-and-Agendas). Learn more about goal setting by taking the [Setting Team and Employee Goals using SMART Methodology LinkedIn Learning course](https://www.linkedin.com/learning/setting-team-and-employee-goals-using-smart-methodology/how-to-use-smart-goals-2?u=2255073)_

#### What is your purpose?

Purpose is value-driven and based on [GitLab values](/handbook/values/) and personal values. Determine what values drive you to be part of a mentorship and complete the following statement:

**I'm a `mentor/mentee` because I care about `value`. `Value` drives me to be a `mentor/mentee` because `your why`.**

**Connecting mentorship and GitLab values**

| Value | Connection to mentorship |
| ----- | --------------- |
| Collaboration | Fosters connection and conversation between team members |
| Results | Driven by specific goals |
| Efficiency | Focused transfer of experience and skill |
| Diveristy, Inclusion, and Belonging | All perspecives, experiences, and backgrounds are valued and included |
| Iteration | Incremental changes to goals are made as relationships develop |
| Transparency | Trust is built between mentor/menee |

#### Communicate

A discussion about goals and purpose should be part of the very first mentorship meeting. Progress and iterations on goals should be revisited during each mentorship meeting.

### Mentee: What to do before your mentorship begins

1. Set up a coffee chat with your mentor. Take time to get to know them and [build trust](/handbook/leadership/building-trust/). Discuss communication styles and preferences
1. Set a specific goal to achieve during your mentorship. It's OK if you don't reach 100% of your goal during the mentorship.
1. Watch this short LinkedIn video on [understanding strengths and weaknesses](https://www.linkedin.com/learning/management-tips/understanding-strengths-and-weaknesses?u=2255073)
1. Take the [16 Personalities test](https://www.16personalities.com/free-personality-test) and share the results with your mentor/mentee. This can help set expectations for effective communication.
1. Join the [#mentoring](https://app.slack.com/client/T02592416/C01QKNDJ76J/thread/C5P8T9VQX-1587584276.009700) Slack channel

### Setting a Meeting Schedule

It's important to set clear expectations with your mentor/mentee about when, how often, and for how long you will formally meet. Below is a suggested format for this meeting cadence:

- Establish a 3 to 6 month window for your mentor/mentee relationship
- Set up a meeting cadence every other week for a 30-45 minute time period
- When the 3 month window concludes, set new expectations for how your mentorship will continue. Some options include:
     - Restart this structure and continue meeting formally for another 3 months
     - Define a new, more casual meeting cadence. This could be 1 meeting every 6-8 weeks or happen asynchronously
     - Decide to end the mentorship and consider staying connected via coffee chats or informal messages on Slack

#### Sample meeting agendas

Setting an agenda for your mentorship sessions is important for resource documentation and future planning. The mentee should be the DRI for each session and use the agenda to set meeting goals and ask questions. Open a new document and share the document with both the mentor/mentee. Use the following meeting agenda templates as a baseline for each session. The template can be customized to meet the needs of the relationship, and might adapt over time.

Sample agendas below are inspired by resources from [Arizona State University](https://wms.arizona.edu/sites/default/files/Sample%20Meeting%20Agendas%20and%20Emails%20for%20Mentors.pdf) and [People Grove](https://support.peoplegrove.com/hc/en-us/articles/360001265792-Structure-Goals-and-Agendas).

***Initial Meeting Agenda***

```
Date: XXXX-XX-XX

Agenda:

- Discuss and share current workplace challenges the mentee is facing. Potential discussion questions (for the mentee to answer) to guide this conversation include:

     - What part of your role makes you feel the most accomplished/present in your work?
     - What are your biggest strengths? What skills and talents do you bring to your team?
     - What is a challenge you're currently facing?
     - What part of your role makes you feel disconnected? 
     - How much of your time each week is spent in this disconnected work?
     - How does your current role fit into your professional development goals?
     - What is the mentee trying to achieve? What is the desired outcome?
     - What is one piece of this challenge that you believe we can tackle in 3 months?

- Set a first-draft goal or intention for the mentorship relationship using the SMART goal formula. Refer to [page 11 in this resources from the University of California](https://www.linkedin.com/learning/setting-team-and-employee-goals-using-smart-methodology/how-to-use-smart-goals-2?u=2255073) for questions you can ask to build your SMART goal

Action Items:

- Mentee will iterate on their SMART goal and communicate the final version to their mentor asynchronously before their next meeting

```

***Regular Meeting Agenda***

```
Date: XXXX-XX-XX

Agenda:

- Review and assess SMART goal
- Potential discussion questions
     - How are you feeling this week/month/quarter?
     - Share successes/actions taken towards reaching SMART goal
     - Discuss current blockers, challenges, questions, or frustrations faced by the mentee
     - Share and brainstorm actions/strategies to address SMART goal
- Set action items for the mentor and mentee for the next session
- Reserve space during the session to share and/or ask for feedback for both the mentor and mentee

Action Items:

- Action item 1
- Action item 2

```

***Final Session Meeting Agenda***

```
Date: XXXX-XX-XX

Agenda:

- Take time to reflect on 3 month mentorship relationship

     - What worked? What was challenging?
     - Provide feedback 1:1 from both the mentor/mentee

- Create an action plan moving forward

     - Discuss how the mentee will continue to work to/refine SMART goal
     - Determine cadence and structure in which the mentor/mentee will continue to collaborate. 

Action Items:

- Mentee will reach out to schedule a meeting based on the expectations set

```

### Career Development

Career development is a major driver of mentee goals during mentorship. Review GitLab resources for career development in our handbook:

1. Self-paced training available in GitLab Learn as part of 2021-08 Skill of the Month on [career development](https://gitlab.edcast.com/channel/skill-of-the-month-fy22)
1. [Handbook documentation](/handbook/people-group/learning-and-development/career-development/) of GitLab career development
1. How to create and use an [individual growth plan](/handbook/people-group/learning-and-development/career-development/#individual-growth-plan)
1. Use GitLab epics to [track and outline career development goals](/handbook/people-group/learning-and-development/career-development/#use-gitlab-epics-to-track-your-career-development)

### Additional discussion resources

These resources are meant to provide both mentors and mentees with additional personal and professional development. Consider reviewing these resources asynchronously and discuss/debrief them during a session with your mentor/mentee.

***Why the Power of Mentoring can Change the World***
<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/u4kTlK5mUHc" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->




