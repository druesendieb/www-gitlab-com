---
layout: handbook-page-toc
title: AI Assist Single-Engineer Group
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## AI Assist Single-Engineer Group

The AI Assist SEG is a [Single-Engineer Group](/company/team/structure/#single-engineer-groups) within our [Incubation Engineering Department](/handbook/engineering/incubation/).

Our aim is to use machine learning models to analyze publically available code across a range of common programming languages to determine the most effective secure coding practices.  We will identify when users write functions that could benefit from our analysis, and offer suggestions on how the user can improve their code to guard against known vulnerabilities and exploits.

This SEG will work closely with our [Applied ML](https://about.gitlab.com/handbook/engineering/development/modelops/appliedml/) group which is building up the data sets that will power these suggestions.



