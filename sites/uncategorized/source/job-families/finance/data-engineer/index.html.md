---
layout: job_family_page
title: "Data Engineering"
---

## Data Engineer

This role requires an analytical and business-oriented mindset with the ability to implement rigorous database solutions and best practices in order to produce and influence the adoption of strong quality data insights to drive business decisions in all areas of GitLab. Data Engineers are essentially software engineers who have a particular focus on data movement and orchestration.

### Job Grade

The Data Engineer is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Requirements

* 2+ years hands-on experience deploying production quality code
* Professional experience using Python, Java, or Scala for data processing (Python preferred)
* Knowledge of and experience with data-related Python packages
* Demonstrably deep understanding of SQL and analytical data warehouses (Snowflake preferred)
* Hands-on experience implementing ETL (or ELT) best practices at scale.
* Hands-on experience with data pipeline tools (Airflow, Luigi, Azkaban, dbt)
* Strong data modeling skills and familiarity with the Kimball methodology.
* Experience with Salesforce, Zuora, Zendesk and Marketo as data sources and consuming data from SaaS application APIs.
* Share and work in accordance with [our values](/handbook/values/)
* Constantly improve product quality, security, and performance
* Desire to continually keep up with advancements in data engineering practices
* Catch bugs and style issues in code reviews
* Ship small features independently
* Successful completion of a background check -- see the [GitLab Code of Business Conduct and Ethics](https://ir.gitlab.com/static-files/7d8c7eb3-cb17-4d68-a607-1b7a1fa1c95d).
* Ability to use GitLab

### Responsibilities

* Maintain our data warehouse with timely and quality data
* Build and maintain data pipelines from internal databases and SaaS applications
* Create and maintain architecture and systems documentation
* Write maintainable, performant code
* Implement the [DataOps](https://en.wikipedia.org/wiki/DataOps) philosophy in everything you do
* Plan and execute system expansion as needed to support the company's growth and analytic needs
* Collaborate with Data Analysts to drive efficiencies for their work
* Collaborate with other functions to ensure data needs are addressed
* This position is always [central](/handbook/business-ops/data-team/#-team-organization) and reports to the Manager, Data

## Senior Data Engineer

The Senior Data Engineer role extends the [Data Engineer](#responsibilities) role.

### Job Grade

The Senior Data Engineer is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

* All requirements of an Intermediate Data Engineer
* Understand and implement data engineering best practices
* Improve, manage, and teach standards for code maintainability and performance in code submitted and reviewed
* Create smaller merge requests and issues by collaborating with stakeholders to reduce scope and focus on iteration
* Ship medium to large features independently
* Generate architecture recommendations and the ability to implement them
* Great communication: Regularly achieve consensus amongst teams
* Perform technical interviews

## Performance Indicators (PI)

*   [SLO achievement per data source](/handbook/business-ops/metrics/#slo-achievement-per-data-source)
*   [Infrastructure Cost vs Plan](/handbook/business-ops/metrics/#infrastructure-cost-vs-plan)
*   [Number of days since last environment audit](/handbook/business-ops/metrics/#number-of-days-since-last-environment-audit)
*   [Mean Time between Failures (MTBF)](/handbook/business-ops/metrics/#mean-time-between-failures-mtbf)
*   [Mean Time to Repair (MTTR)](/handbook/business-ops/metrics/#mean-time-to-repair-mttr)

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team).

* Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with one of our Global Recruiters
* Next candidates will be invited to complete an assessment
* On successful completion of the assessment, candidates will then be invited to schedule a first interview with the Hiring Manager
* Candidates will then be invited to schedule two separate interviews with two  Peers from the Data team
* Final stage will be with our Senior Director of Data. 

Additional details about our process can be found on our [hiring page](/handbook/hiring/).

### Career Ladder

The next step in the Data Engineer job family is to move to the [Data Management](/job-families/finance/manager-data/) job family. 
